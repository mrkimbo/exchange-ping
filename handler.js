const fetch = require('node-fetch');
const IFTTT = require('node-ifttt-maker');

const { MAKER_KEY, SYMBOL } = process.env;

const dispatcher = new IFTTT(MAKER_KEY);
const matcher = new RegExp(SYMBOL, 'i');
const EVENT_NAME = 'Symbol:Found';
const URLS = [
  'https://api.kucoin.com/v1/market/open/coins',
  'https://api.binance.com/api/v1/ticker/price'
];

function parseKuCoin(json) {
  return json.data.some((item) => (
    matcher.test(item.coin)
  ));
}

function parseBinance(json) {
  return json.some((item) => (
    matcher.test(item.symbol)
  ));
}

function load() {
  return Promise.all(URLS.map((url) => (
    fetch(url)
      .then((response) => response.json())
      .then((json) => {
        if ((/kucoin/i).test(url)) {
          return Promise.resolve({ kucoin: parseKuCoin(json) });
        }
        if ((/binance/i).test(url)) {
          return Promise.resolve({ binance: parseBinance(json) })
        }
      })
  )));
}

module.exports.search = (event, context, callback) => {
  console.log(`Checking Binance and KuCoin for symbol: '${SYMBOL}'`);
  
  load().then((results) => {
      let found = false;
      results.forEach((item) => {
        const name = Object.keys(item)[0];
        if (!found && item[name] === true) {
          found = true;
          const params = {
            value1: SYMBOL.toUpperCase(),
            value2: Object.keys(item)[0]
          };
          dispatcher.request({ event: EVENT_NAME, params });
        }
      });
      
      console.log(`Symbol ${found ? 'found' : 'not found'}`, results);
      callback(null, { message: results });
      
    })
    .catch(({ message }) => {
      callback(null, { message });
    });
};
