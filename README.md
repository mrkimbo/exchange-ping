# Exchange Ping

#### Summary

Quick project to ping a couple of cryptocurrency exchanges to check for the listing of a particular currency symbol.

Initially implemented as a parameterised endpoint on an express server, but pivoted to
re-implement as an AWS lambda function, triggered by a CloudWatch schedule event.

Used Serverless framework to automate cloudformation script - provisioning and deployment.



